<div class="row p-3" id="footer">
    <div class="col-sm-3 col-md-4 col-lg-4"></div>
    <div class="col-sm-6 col-md-4 col-lg-4 text-center textExtraSmall" style="color: #707070">
        <div class="mt-3">
            Dra. Daniella Prudente – Todos os direitos reservados 2024
        </div>
        <div class="mt-3">
            <span class="font-weight-bolder">Dp Educacao Medica Digital LTDA</span>
            – CNPJ: 41.429.442/0001-35
        </div>
        <div class="mt-3">
            <u>Política de Privacidade</u>
        </div>
    </div>
    <div class="col-sm-3 col-md-4 col-lg-4"></div>
</div>