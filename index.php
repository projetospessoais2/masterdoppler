<!DOCTYPE html>
<html lang="pt-BR">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="initial-scale=1" />

        <link rel="icon" type="image/x-icon" href="assets/favicon.ico">
        <title>Doppler nas Lesões Ovarianas | Cadastro V5 – Dra. Daniella Prudente</title>

        <!-- CSS -->
        <!-- <link rel="stylesheet" href="css/fonts.css">
        <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
        
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/form.css">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        
        <link rel="stylesheet" href="css/responsividade.css"> -->
        <link rel="stylesheet" href="css/used.css">
    </head>

    <body style="background-color: black">

        <?php require('contents/section_1.php'); ?>
        <?php require('contents/section_2.php'); ?>
        <?php require('contents/section_3.php'); ?>
        <?php require('contents/section_4.php'); ?>
        <?php require('contents/section_5.php'); ?>
        <?php require('contents/section_6.php'); ?>

        <?php require('contents/formulario.php'); ?>

        <script src="js/activeCampaign.js"></script>

    </body>

    <?php require('default/footer.php'); ?>

</html>