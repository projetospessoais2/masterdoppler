<?php $todaysDate = date("Y-m-d H:i:s"); ?>

<div class="modal">
    <div class="modal_content">
        <span class="close">&times;</span>
        
        <div class="row mt-5">
            <div class="col-1 col-md-3"></div>
            <div class="col-10 col-md-6 text-center">
                <img src="./assets/logo.webp" class="card-img logoSectionFinal" width="80%">
            </div>
            <div class="col-1 col-md-3"></div>
        </div>

        <div class="row mt-45">
            <div class="col-1"></div>
            <div class="col-10">
                <div class="textBigger font-weight-bolder text-center text-white mt-4">
                    Preencha o formulário obrigatório para continuar:
                </div>
            </div>
            <div class="col-1"></div>
        </div>
        <form method="POST" action="https://kleberpimentel.activehosted.com/proc.php" id="_form_3_" class="_form _form_3 _inline-form  _dark" novalidate data-styles-version="4">
            <input type="hidden" name="u" value="3" />
            <input type="hidden" name="f" value="3" />
            <input type="hidden" name="s" />
            <input type="hidden" name="c" value="0" />
            <input type="hidden" name="m" value="0" />
            <input type="hidden" name="act" value="sub" />
            <input type="hidden" name="v" value="2" />
            <input type="hidden" name="or" value="065a5c903ca8b6742d600acef7f77f6d" />

            <input type="text" id="fullname" name="fullname" class="form-control inputFormActive mt-4" placeholder="Nome">
            <input type="text" id="email" name="email" class="form-control inputFormActive mt-3" placeholder="E-mail">
            <input type="phone" id="phone" name="phone" class="form-control inputFormActive mt-3" placeholder="Celular (ex: 11999999999)" pattern="^(\(11\) [9][0-9]{4}-[0-9]{4})|(\(1[2-9]\) [5-9][0-9]{3}-[0-9]{4})|(\([2-9][1-9]\) [5-9][0-9]{3}-[0-9]{4})$">
            <input type="text" id="field[8]" name="field[8]" class="form-control inputFormActive mt-3" placeholder="UF/CRM">

            <!-- UTMS -->
            <input type="hidden" id="field[2]" name="field[2]" value="" placeholder=""/>
            <input type="hidden" id="field[3]" name="field[3]" value="" placeholder=""/>
            <input type="hidden" id="field[4]" name="field[4]" value="" placeholder=""/>
            <input type="hidden" id="field[5]" name="field[5]" value="" placeholder=""/>
            <input type="hidden" id="field[6]" name="field[6]" value="" placeholder=""/>

            <!-- DATA -->
            <input type="hidden" id="field[15][time]" name="field[15][time]" value="<?= $todaysDate ?>" placeholder=""/>

            <div class="text-center mt-4 mb-5">
                <button type="submit" id="_form_3_submit" class="btn btn-block font-weight-bolder">QUERO PARTICIPAR</button>
            </div>
            
        </form>

    </div>
</div>

<script>
    
const buttons = document.querySelectorAll("#btnParticipar")
const modal = document.querySelector(".modal")
const closeBtn = document.querySelector(".close")

document.addEventListener("DOMContentLoaded",() => {
    buttons.forEach((button) => {
        button.addEventListener("click", handleClick);
    });
})

function handleClick(event) {
  modal.style.display = "block";
  closeBtn.addEventListener("click", () => {
    modal.style.display = "none"
  })
}

</script>