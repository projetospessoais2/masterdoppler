<div style="background-color: black">

    <div class="row">
        <div class="col-2 col-md-4 col-lg-5"></div>
        <div class="col-8 col-md-4 col-lg-2 mt-5">
            <img src="assets/logo.webp" alt="" class="card-img">
        </div>
        <div class="col-2 col-md-4 col-lg-5"></div>
    </div>

    <div class="text-center mb-5">
        <div class="mt-1">
            <?php require('./contents/bloco_data.php'); ?>
        </div>
    
        <div class="mt-4 d-none d-lg-block">
            <?php $btnText = 'GARANTIR MINHA VAGA GRATUITA'; ?>
            <?php require('./contents/btn_participar.php'); ?>
        </div>

        <!-- MOBILE -->
        <div class="row mt-4 d-lg-none">
            <div class="col-1"></div>
            <div class="col-10">
                <?php $btnText = 'GARANTIR MINHA VAGA GRATUITA'; ?>
                <?php require('./contents/btn_participar.php'); ?>
            </div>
            <div class="col-1"></div>
        </div>
    </div>

    <div class="mb-4"></div>

</div>