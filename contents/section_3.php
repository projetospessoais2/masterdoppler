<div id="section3e5">
    <div class="row">
        <div class="col-1 col-md-2"></div>
        <div class="col-10 col-md-8 mb-2">
            <div class="mt-5 text-center textBigger font-weight-bolder">
                Como vai funcionar?
            </div>

            <span class="font-weight-light textNormal">
                <div class="mt-4">
                    A Masterclass Doppler nas Lesões Ovarianas vai acontecer, ao vivo, no dia 25 de janeiro, às 20h e é EXCLUSIVA para médicos.
                </div>
    
                <div class="mt-3">
                    Vamos compartilhar com você como você pode dominar o doppler nas lesões ovarianas e saber como laudar com mais segurança.
                </div>
    
                <div class="mt-3">
                    Durante toda a nossa masterclass, você terá acesso à junção da melhor teoria com a vivência prática e se sentirá muito mais seguro para avaliar e laudar lesões ovarianas
                </div>
            </span>

            <div class="mt-2"></div>
            <div class="row mt-4">
                <div class="col-md-7 text-center">
                    <div id="cardAprendizados" class="textNormal">
                        DOPPLER NAS LESÕES OVARIANAS
                    </div>
                </div>
                
                <div class="col-md-5 marginS3CardMobile text-center">
                    <div id="cardAprendizados" class="textNormal">
                        <span class="">
                            CLAREZA PARA AVALIAR
                        </span>
                    </div>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-md-7 text-center">
                    <div id="cardAprendizados" class="textNormal">
                        ULTRASSOM COM MAIS SEGURANÇA
                    </div>
                </div>
                
                <div class="col-md-5 marginS3CardMobile text-center">
                    <div id="cardAprendizados" class="textNormal">
                        <span class="">
                            CASOS PRÁTICOS
                        </span>
                    </div>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-md-7 text-center">
                    <div id="cardAprendizados" class="textNormal">
                        DESCREVA COM EXATIDÃO O QUE SE VÊ NA IMAGEM
                    </div>
                </div>
                
                <div class="col-md-5 marginS3CardMobile text-center">
                    <div id="cardAprendizados" class="textNormal">
                        <span class="">
                            CONTEÚDO EXCLUSIVO
                        </span>
                    </div>
                </div>
            </div>

            <?php $btnText = 'QUERO PARTICIPAR' ?>
            <div class="mt-4 text-center">
                <div class="textNormal">
                    Ter esse conhecimento é a possibilidade de se destacar da maioria e se tornar uma referência médica.
                </div>
                <div class="mt-4 mb-5">
                    <?php require('./contents/btn_participar.php'); ?>
                </div>
            </div>
    
        </div>
        <div class="col-1 col-md-2"></div>
    </div>
</div>