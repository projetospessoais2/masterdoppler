<div id="section1">
    <div class="row">
        <div class="col-1"></div>
        <div class="col-10 text-center text-lg-start col-lg-4 mt-5 mb-5 text-white columnS1Mobile">

            <div class="mt-5"></div>
            <div class="row">
                <div class="col-md-2 d-none d-md-block d-lg-none"></div>
                <div class="col-md-8 col-lg-8">
                    <img src="./assets/logo.webp" alt="" class="card-img iconLogoMobile">
                </div>
                <div class="col-md-2"></div>
            </div>

            <div class="font-weight-bolder mt-4 textNormal">
                EVENTO EXCLUSIVO PARA MÉDICOS
            </div>

            <?php require('./contents/bloco_data.php'); ?>

            <div class="mt-4 textBigger font-weight-bolder lh-1-3">
                Tenha mais segurança para avaliar e laudar as lesões ovarianas e se torne uma referência profissional na sua cidade e região.
            </div>

            <?php $btnText = 'CLIQUE PARA PARTICIPAR' ?>
            <div class="mt-4">
                <?php require('./contents/btn_participar.php'); ?>
            </div>

            <div class="mt-4 font-weight-bold textSmall">
                AO VIVO | 100% ONLINE E GRATUITO
            </div>

        </div>
        <div class="col-1 col-lg-7"></div>
    </div>
</div>