<div id="section3e5">
    <div class="row">

        <!-- DESKTOP -->
        <div class="d-none d-lg-block col-lg-2 order-1"></div>
        <div class="d-none d-lg-block col-lg-4 order-md-3 order-lg-2 mt-5 text-center">
            <img src="assets/daniella.webp" alt="" class="card-img">
        </div>
        <div class="d-none d-lg-block col-lg-4 order-md-2 order-lg-3 d-flex align-self-center flex-column textNormal mb-4">
            <img src="assets/dani_nome.webp" alt="" class="card-img">
            <div class="mt-4">
                Médica Radiologista com Título de Especialista em Radiologia e Diagnóstico por Imagem pelo MEC, especialista em Ultrassonografia Geral pelo Colégio Brasileiro de Radiologia, Diretora Médica da Clínica IRBA – Instituto de Radiodiagnóstico da Bahia e professora da CALIPER Escola de Imagem.
            </div>
            <div class="mt-3">
                Conta com mais de 27 anos de experiência em métodos ligados à saúde da mulher, com ênfase em Mamografia e Ultrassonografia mamária. Palestrante em diversos congressos nacionais e eventos online.
            </div>
        </div>
        <div class="d-none d-lg-block col-lg-2 order-4">
        </div>

        <!-- MOBILE -->
        <div class="col-1 d-lg-none"></div>
        <div class="col-10 d-lg-none">
            <img src="assets/daniella.webp" alt="" class="card-img">
            <img src="assets/dani_nome.webp" alt="" class="card-img">
            <div class="mt-4">
                Médica Radiologista com Título de Especialista em Radiologia e Diagnóstico por Imagem pelo MEC, especialista em Ultrassonografia Geral pelo Colégio Brasileiro de Radiologia, Diretora Médica da Clínica IRBA – Instituto de Radiodiagnóstico da Bahia e professora da CALIPER Escola de Imagem.
            </div>
            <div class="mt-3">
                Conta com mais de 27 anos de experiência em métodos ligados à saúde da mulher, com ênfase em Mamografia e Ultrassonografia mamária. Palestrante em diversos congressos nacionais e eventos online.
            </div>
        </div>
        <div class="col-1 d-lg-none"></div>
    </div>

    <div class="row">
        <!-- DESKTOP -->
        <div class="d-none d-lg-block col-lg-2 order-1"></div>
        <div class="d-none d-lg-block col-lg-4 order-md-3 order-lg-2 d-flex align-self-center flex-column textNormal mb-4">
            <img src="assets/kleber_nome.webp" alt="" class="card-img">
            <div class="mt-4">
                Médico pela UFBA com Título de Especialista em Ultrassonografia Geral, Ginecologia e Obstetrícia e em Medicina Fetal e Mestre em Medicina e Saúde pela UFBA Nível 6 CAPES.
            </div>
            <div class="mt-3">
                Membro da International Society of Ultrasound in Obstetrics and Gynecology (ISUOG).
            </div>
            <div class="mt-3">
                Professor adjunto de Obstetrícia da UFBA. 
            </div>
            <div class="mt-3">
                Atualmente, conta com centenas de alunos pelo Brasil no Curso Morfológico do Primeiro Trimestre.
            </div>
        </div>
        <div class="d-none d-lg-block col-lg-4 order-md-2 order-lg-3 mt-5 text-center">
            <img src="assets/kleber.webp" alt="" class="card-img">
        </div>
        <div class="d-none d-lg-block col-lg-2 order-4">
        </div>

        
        <!-- MOBILE -->
        <div class="col-1 d-lg-none"></div>
        <div class="col-10 d-lg-none mb-5">
            <img src="assets/kleber.webp" alt="" class="card-img">
            <img src="assets/kleber_nome.webp" alt="" class="card-img">
            <div class="mt-4">
                Médico pela UFBA com Título de Especialista em Ultrassonografia Geral, Ginecologia e Obstetrícia e em Medicina Fetal e Mestre em Medicina e Saúde pela UFBA Nível 6 CAPES.
            </div>
            <div class="mt-3">
                Membro da International Society of Ultrasound in Obstetrics and Gynecology (ISUOG).
            </div>
            <div class="mt-3">
                Professor adjunto de Obstetrícia da UFBA. 
            </div>
            <div class="mt-3">
                Atualmente, conta com centenas de alunos pelo Brasil no Curso Morfológico do Primeiro Trimestre.
            </div>
        </div>
        <div class="col-1 d-lg-none"></div>

    </div>

</div>