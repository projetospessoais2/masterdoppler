<div class="p-4" style="background-color: #0c0c0c">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8 text-center d-relative divSection2Animation">
            <span class="textBigger lh-1-3 font-weight-bolder section2Animation">
                Tenha segurança para avaliar e laudar lesões ovarianas através do Doppler.
            </span>
        </div>
        <div class="col-md-2"></div>
    </div>
</div>