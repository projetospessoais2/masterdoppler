<div style="background-color: black">
    <div class="row">

        <!-- DESKTOP -->
        <div class="d-none d-md-block col-md-1"></div>
        <div class="d-none d-md-block col-md-5 mt-5 text-center">
            <div class="textBigger font-weight-bolder">
                Para quem é este evento?
            </div>
            <div class="textNormal mt-4">
                Toda a nossa Masterclass será focada em assuntos técnicos e conteúdos direcionados para:
            </div>
        </div>
        <div class="d-none d-md-block col-md-5 mt-5">
            <div class="textNormal">
                <span> <i class="fas fa-check faIcon"></i> </span> &nbsp;
                Radiologistas
            </div>
            <div class="textNormal mt-2">
                <span> <i class="fas fa-check faIcon"></i> </span> &nbsp;
                Ultrassonografistas
            </div>
            <div class="textNormal mt-2">
                <span> <i class="fas fa-check faIcon"></i> </span> &nbsp;
                Clínicos
            </div>
            <div class="textNormal mt-2">
                <span> <i class="fas fa-check faIcon"></i> </span> &nbsp;
                Oncologistas
            </div>
            <div class="textNormal mt-2">
                <span> <i class="fas fa-check faIcon"></i> </span> &nbsp;
                Geriatras
            </div>
            <div class="textNormal mt-2">
                <span> <i class="fas fa-check faIcon"></i> </span> &nbsp;
                Endocrionologistas
            </div>
        </div>
        <div class="d-none d-md-block col-md-1"></div>

        <!-- MOBILE -->
        <div class="text-center d-md-none mt-5">
            <div class="textBigger font-weight-bolder">
                Para quem é este evento?
            </div>
            <div class="textNormal mt-4">
                Toda a nossa Masterclass será focada em assuntos técnicos e conteúdos direcionados para:
            </div>
        </div>

        <div class="col-1 d-md-none"></div>
        <div class="col-10 d-md-none mt-4">
            <div class="textNormal">
                <span> <i class="fas fa-check faIcon"></i> </span> &nbsp;
                Radiologistas
            </div>
            <div class="textNormal mt-2">
                <span> <i class="fas fa-check faIcon"></i> </span> &nbsp;
                Ultrassonografistas
            </div>
            <div class="textNormal mt-2">
                <span> <i class="fas fa-check faIcon"></i> </span> &nbsp;
                Clínicos
            </div>
            <div class="textNormal mt-2">
                <span> <i class="fas fa-check faIcon"></i> </span> &nbsp;
                Oncologistas
            </div>
            <div class="textNormal mt-2">
                <span> <i class="fas fa-check faIcon"></i> </span> &nbsp;
                Geriatras
            </div>
            <div class="textNormal mt-2">
                <span> <i class="fas fa-check faIcon"></i> </span> &nbsp;
                Endocrionologistas
            </div>

        </div>
        <div class="col-1 d-md-none"></div>
    </div>

    <div class="text-center mt-5 mb-5">
        <?php require('./contents/btn_participar.php'); ?>
    </div>
</div>